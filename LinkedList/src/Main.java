public class Main {
	public static void main (String[]args){
		
		LinkedList<Object> l = new LinkedList<Object>();
		
		System.out.println("List : " + l.toString());
		System.out.println("Length : " + l.getLength());
		System.out.println();
		
		l.add("String");
		l.add(3.9d);
		l.add(1.0f);
		l.add(7);
		
		System.out.println("List : " + l.toString());
		System.out.println("Length : " + l.getLength());
		System.out.println();
		
		l.add(0, 5);
		l.add(4, "Hej");
		l.add(5, 3.0d);
		l.add(6, 1.0f);
		l.add(7, 4);
				
		System.out.println("List : " + l.toString());
		System.out.println("Length : " + l.getLength());
		System.out.println();
		
		l.remove(7);
		l.remove(6);
		l.remove(5);
		l.remove(4);
		l.remove(0);
		System.out.println();
		
		System.out.println("List : " + l.toString());
		System.out.println("Length : " + l.getLength());
		
	}
}
