public class LinkedList<T> {
	
	Node current;
	Node head;
	int itemCount;
	
	public LinkedList(){
		head = null;
		itemCount = 0;
	}
	
	public void add(T data){
		if(head == null){
			head = new Node(data);
		}
		else{
			current = head;
			while(current.getNext() != null){
				
				current = current.getNext();
				
			}
			current.setNext(new Node(data));
			current = current.getNext();
		}
		itemCount++;
	}
	
	public void remove(int place){
		current = head;
		
		if(place == 0){
			System.out.println("Removed : " + head.data);
			head = head.getNext();
			itemCount--;
			return;
		}
		
		for(int i = 0; i < (place - 1); i++){
			current = current.getNext();
		}
		Node next = current.getNext();
		current.setNext(next.getNext());
		current = current.getNext();
		System.out.println("Removed : " + next.data);
//		next.setNext(null); beh�vs ej f�r gc tar bort allt som inte har n�got som pekar p� dem
		itemCount--;
	}
	
	public void add(int place, T data){
		current = head;
		if(place == 0){
			Node p = current;
			head = new Node(data);
			head.setNext(p);
			current = current.getNext();
			itemCount++;
			return;
		}
		for(int i = 0; i < (place - 1); i++){
			current = current.getNext();
		}
		Node p = current.getNext();
		current.setNext(new Node(data));
		current = current.getNext();
		current.setNext(p);
		itemCount++;
	}
	
	public int getLength(){
		return itemCount;
	}
	
	public String toString(){
		String utString = "";
		current = head;
		if(head == null){
			return "list is empty";
		}
		
		while(current != null){
			utString += current.getData() + " ";
			current = current.getNext();
		}
		
		
		return utString;
	}
	
	private class Node{
		Node next;
		T data;
		
		public Node(T data){
			this.data = data;
			this.next = null;
		}
		
		public void setNext(Node next){
			this.next = next;
		}
		
		public T getData(){
			return data;
		}
		
		public Node getNext(){
			return next;
		}
	}
}